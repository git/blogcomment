#!/usr/bin/env python

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__  = "Jose Manuel Diez"
__email__   = "me@jdiez.me"
__license__ = "GPL"

import cookielib
import urllib
import urllib2
import sys
import json
import argparse

jar = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(jar))

def login(email, password):
    data = urllib.urlencode({
        'returnUrl': 'http://www.theguardian.com/',
        'authFailedReturnUrl': 'http://www.theguardian.com/',
        'emailAddress': email,
        'password': password,
        'keepMeSignedIn': 'on'
    })

    response = opener.open('https://id.theguardian.com/signin', data).read()

    if "Authentication failed" in response:
        return False

    return True

def extract_id(url):
    response = opener.open(url).read()

    left = response.split('<link rel="shorturl" href="http://gu.com/p/')
    article_id = left[1].split('" />')[0]

    return article_id

def comment(article_id, comment_text):
    if len(comment_text) > 5000:
        print "Comment length is over 5000. Please shorten."
        return false
    
    data = urllib.urlencode({
        'body': comment_text
    })
    import pprint
    pprint.pprint(('http://discussion.theguardian.com/api/discussion/p/' + article_id + '/comment', data))
    response = opener.open('http://discussion.theguardian.com/api/discussion/p/' + article_id + '/comment', data).read()
    response = json.loads(response)
    
    if response['statusCode'] != 200:
        print "Comment failed. Status: " + response['status']
        return False
    else:
        print "Comment successfully posted. Message id: " + response['message']
        return True

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Post comments to The Guardian.")
    parser.add_argument('email', help='The email address with which the account is registered.')
    parser.add_argument('password', help='The password of the account.')
    parser.add_argument('url', help='The article URL.')
    args = parser.parse_args()

    if not login(args.email, args.password):
        print "Authentication failure."
        sys.exit()
    
    print "Authentication successful."
    try:
        article_id = extract_id(args.url)
    except:
        print "Unable to extract article id. Is the URL valid?"
        sys.exit()   

    print "Extracted article id."
    print "Please input your comment. Terminate your input with an EOF character (Ctrl-D)."
    text = ''.join(sys.stdin.readlines())
    comment(article_id, text)
